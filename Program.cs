﻿using System;

namespace ShapeArea
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            System.Console.WriteLine(shapeArea(n));
        }

        static int shapeArea(int n) {
            int area = 1;

            for (int i = 1; i < n; i++)
            {
                area += 4 * i;
            }
            return area;

        }

    }
}
